import Vue from 'vue'
import App from './App.vue'
import Signin from './components/Signin.vue'
import Signup from './components/Signup.vue'
import MainPage from './components/MainPage.vue'

Vue.component('vue-signin', Signin);
Vue.component('vue-signup', Signup);
Vue.component('vue-mainpage', MainPage);

new Vue({
  el: '#app',
  render: h => h(App)
})
